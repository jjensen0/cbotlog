/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "capi324v221.h"
#include <string.h>

//#define DEBUG
#define RTC_ADDR 0x68 // DS1307 RTC Slave address (1101000)
#define EEPROM_ADDR 0x50 // 1010000 - AT24C32 1010 is the device 000 is the number on the bus, up to 8 total.
inline __attribute__((gnu_inline)) void OW_delay(uint16_t delay) {
	while (delay--)
		asm volatile("nop");
}

int BCDtoINT(unsigned char var);
unsigned char INTtoBCD(int var);

#define LOOP_CYCLES 8 //Number of cycles that the loop takes
#define us(num) (num/(LOOP_CYCLES*.05))

#define OW_PORT PORTA
#define OW_DDR DDRA
#define OW_PIN PINA
#define OW_DQ PA3

#define OW_INPUT_MODE() OW_DDR &=~ (1<<OW_DQ)
#define OW_OUTPUT_MODE() OW_DDR |= (1<<OW_DQ)
#define OW_LOW() OW_PORT &= ~(1<<OW_DQ)
#define OW_HIGH() OW_PORT |= (1<<OW_DQ)

#define OW_DECIMAL_STEPS_12BIT 625 //.0625
uint8_t OW_reset();
void OW_write_bit(uint8_t bit);
void OW_write_byte(uint8_t byte);
uint8_t OW_read_bit(void);
uint8_t OW_read_byte(void);
void prompt_erase();

#define OW_CMD_CONVERTTEMP 0x44
#define OW_CMD_RSCRATCHPAD 0xbe
#define OW_CMD_WSCRATCHPAD 0x4e
#define OW_CMD_CPYSCRATCHPAD 0x48
#define OW_CMD_RECEEPROM 0xb8
#define OW_CMD_RPWRSUPPLY 0xb4
#define OW_CMD_SEARCHROM 0xf0
#define OW_CMD_READROM 0x33
#define OW_CMD_MATCHROM 0x55
#define OW_CMD_SKIPROM 0xcc
#define OW_CMD_ALARMSEARCH 0xec

#define CtoF(value) ((value * 9/5)+35)

uint8_t temperature[2];
int8_t digit;
// 0-0-0-0-0-0-0-0-0-0-0-0-0-0 Prototypes 0-0-0-0-0-0-0-0-0-0-0-0-0-0-0
void display_time();
void set_time();
void log_data();
void transfer_data();
//int BCDtoINT(unsigned char var);
//unsigned char INTtoBCD(int var);
int getTemp();
BOOL ONEWIRE_reset();
void fix_CH();
void STORE_writes(unsigned char *input, int length);
void init_eeprom();
void reset_eeprom();

unsigned char sec, min, hours, day, date, month, year; // Storing raw reads from RTC NOTE: BCD VALUES!
unsigned char curByteH;
unsigned char curByteL;
unsigned char memLocL[2] = { 0x00, 0x01 };
unsigned char memLocH[2] = { 0x00, 0x00 };
BOOL firstRun = TRUE;

void CBOT_main() {
	ATTINY_open();  //Switches
	LCD_open();     //General usage
	LCD_clear();    //Off-chance anything is on the screen from last flashing.
	I2C_open();
	init_eeprom();
	while (1) {

		display_time();
		if (ATTINY_get_SW_state(ATTINY_SW3)) {
			//Start setting time routine
			set_time();

		} else if (ATTINY_get_SW_state(ATTINY_SW4)) {
			//Start logging data and storing into EEPROM
			log_data();
		} else if (ATTINY_get_SW_state(ATTINY_SW5)) {
			//Start Serial Communication for term
			transfer_data();
		}
	}
}

void fix_CH() {
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00);
		sec &= 0x7f; //01111111
		I2C_MSTR_send(sec);
		I2C_MSTR_stop();
	}
}

void display_time() {
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00); // Tell RTC that we want to start at seconds byte.
	} else { // If RTC is not available, alert user to error and block further execution.
		LCD_printf("Issues communicating\n");
		LCD_printf("with clock.\n\n\n");
		UART_printf(UART_UART0, "Issues with clock.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	}
	//If it reached this point, should be ready to get.
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MR) == I2C_STAT_OK) {
		I2C_MSTR_get(&sec, TRUE);
		I2C_MSTR_get(&min, TRUE);
		I2C_MSTR_get(&hours, TRUE);
		I2C_MSTR_get(&day, TRUE);
		I2C_MSTR_get(&date, TRUE);
		I2C_MSTR_get(&month, TRUE);
		I2C_MSTR_get(&year, FALSE);
		I2C_MSTR_stop();
		if ((sec & 0x80) != 0x00) //Clock halt bit is set, need to fix before continue.
				{
			fix_CH();
			UART_printf(UART_UART0, "Clock Halt register set to 1\n");
		}
		LCD_printf_RC(2, 6,
				"%02d:%02d:%02d\t", BCDtoINT(hours), BCDtoINT(min), BCDtoINT(sec));
		LCD_printf_RC(3, 5,
				"%02d/%02d/20%02d\t", BCDtoINT(month), BCDtoINT(date), BCDtoINT(year));
		switch (day) {
		case 1:
			LCD_printf_RC(1, 0, "       Sunday       ");
			break;
		case 2:
			LCD_printf_RC(1, 0, "       Monday       ");
			break;
		case 3:
			LCD_printf_RC(1, 0, "      Tuesday       ");
			break;
		case 4:
			LCD_printf_RC(1, 0, "     Wednesday      ");
			break;
		case 5:
			LCD_printf_RC(1, 0, "      Thursday      ");
			break;
		case 6:
			LCD_printf_RC(1, 0, "       Friday       ");
			break;
		case 7:
			LCD_printf_RC(1, 0, "      Saturday      ");
			break;
		default:
			LCD_printf_RC(1, 0, "  End of the world  ");
			break;
		}
	}
	if (BCDtoINT(min) % 15 == 0 || firstRun)
		LCD_printf_RC(0, 0, "         %02dF       ", CtoF(getTemp()));
	firstRun = FALSE;
	return;
}

void set_time() {
#ifdef DEBUG
	UART_printf(UART_UART0, "Setting time.\n");
#endif
	int curPos = 1; // 1 = month, 2 day, 3 year, followed by hour, min, sec, day
	int rMonth, rDate, rYear, rHours, rMin, rSec = 0; // Int values of each variable.
	rMonth = BCDtoINT(month);
	rDate = BCDtoINT(date);
	rYear = BCDtoINT(year);
	rHours = BCDtoINT(hours);
	rMin = BCDtoINT(min);
	rSec = BCDtoINT(sec);

	TMRSRVC_delay(500);
	while (1) {
		unsigned char sensors = ATTINY_get_sensors();
		if (sensors & SNSR_SW3_STATE) {
			if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
				I2C_MSTR_send(0x00);
				I2C_MSTR_send(INTtoBCD(rSec));
				I2C_MSTR_send(INTtoBCD(rMin));
				I2C_MSTR_send(INTtoBCD(rHours));
				I2C_MSTR_send(day);
				I2C_MSTR_send(INTtoBCD(rDate));
				I2C_MSTR_send(INTtoBCD(rMonth));
				I2C_MSTR_send(INTtoBCD(rYear));
				I2C_MSTR_stop();
				UART_printf(UART_UART0,
						"Setting time as: %02d/%02d/%02d at %02d:%02d:%02d on day  %d\n",
						rMonth, rDate, rYear, rHours, rMin, rSec, day);
			}
			//Set data and break out.
			break;
		}

		if (sensors & SNSR_SW4_STATE) {
			//Increase by one.
			switch (curPos) {
			case 1:
				rMonth++;
				if (rMonth > 12)
					rMonth = 1;
				break;
			case 2:
				rDate++;
				if ((rMonth == 2 && !(rYear % 4) && rDate > 29)
						|| (rMonth == 2 && rYear % 4 && rDate > 28)
						|| ((rMonth == 1 || rMonth == 3 || rMonth == 5
								|| rMonth == 7 || rMonth == 8 || rMonth == 10
								|| rMonth == 12) && rDate > 31)
						|| ((rMonth == 4 || rMonth == 6 || rMonth == 9
								|| rMonth == 11) && rDate > 30))
					rDate = 1;
				break;
			case 3:
				rYear++;
				if (rYear > 20)
					rYear = 10;
				break;
			case 4:
				rHours++;
				if (rHours > 23)
					rHours = 0;
				break;
			case 5:
				rMin++;
				if (rMin > 59)
					rMin = 0;
				break;
			case 6:
				rSec++;
				if (rSec > 59)
					rSec = 0;
				break;
			case 7:
				day++;
				if (day > 7)
					day = 1;
				break;
			default:
				break;
			}
		}
		if (sensors & SNSR_SW5_STATE) {
			curPos++;
		}
		switch (curPos)
		// Switch to blink current position to indicated what it's changing.
		{
		case 1:
			LCD_printf_RC(3, 5, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(3, 5, "%02d", rMonth);
			TMRSRVC_delay(500);
			break;
		case 2:
			LCD_printf_RC(3, 8, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(3, 8, "%02d", rDate);
			TMRSRVC_delay(500);
			break;
		case 3:
			LCD_printf_RC(3, 13, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(3, 13, "%02d", rYear);
			TMRSRVC_delay(500);
			break;
		case 4:
			LCD_printf_RC(2, 6, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(2, 6, "%02d", rHours);
			TMRSRVC_delay(500);
			break;
		case 5:
			LCD_printf_RC(2, 9, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(2, 9, "%02d", rMin);
			TMRSRVC_delay(500);
			break;
		case 6:
			LCD_printf_RC(2, 12, "  ");
			TMRSRVC_delay(500);
			LCD_printf_RC(2, 12, "%02d", rSec);
			TMRSRVC_delay(500);
			break;
		case 7:
			LCD_printf_RC(1, 0, "                    ");
			TMRSRVC_delay(500);
			switch (day) {
			case 1:
				LCD_printf_RC(1, 0, "       Sunday       ");
				break;
			case 2:
				LCD_printf_RC(1, 0, "       Monday       ");
				break;
			case 3:
				LCD_printf_RC(1, 0, "      Tuesday       ");
				break;
			case 4:
				LCD_printf_RC(1, 0, "     Wednesday      ");
				break;
			case 5:
				LCD_printf_RC(1, 0, "      Thursday      ");
				break;
			case 6:
				LCD_printf_RC(1, 0, "       Friday       ");
				break;
			case 7:
				LCD_printf_RC(1, 0, "      Saturday      ");
				break;
			default:
				LCD_printf_RC(1, 0, "  End of the world  ");
				break;
			}
			TMRSRVC_delay(500);
			break;

		default:
			curPos = 1;
			break;

		}

	}
	return;
}
void prompt_erase() {
	if (SYS_get_state(SUBSYS_LCD) == SUBSYS_CLOSED)
		LCD_open();
	if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED)
		SPKR_open(SPKR_BEEP_MODE);
	LCD_clear();
#ifdef DEBUG
	UART_printf(UART_UART0, "Entering prompt erase.\n");
#endif
	LCD_printf("     EEPROM FULL!    ");
	LCD_printf("   Delete all data?  ");
	LCD_printf("                     ");
	LCD_printf("   Y               N ");
	while (1) {
		if (ATTINY_get_SW_state(ATTINY_SW5)) {
			LCD_clear();
			break;
		} else if (ATTINY_get_SW_state(ATTINY_SW3)) {
			I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
			I2C_MSTR_send(0x00);
			I2C_MSTR_send(0x00);
			I2C_MSTR_send(0x00);
			I2C_MSTR_stop();
			I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
			I2C_MSTR_send(0x00);
			I2C_MSTR_send(0x01);
			I2C_MSTR_send(0x02); //Resetting current log location.
			I2C_MSTR_stop();
			LCD_clear();
			break;
		}
	}
}

char *get_date(char *buf) {
#ifdef DEBUG
	UART_printf(UART_UART0, "Entering get_date.\n");
#endif
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MT) == I2C_STAT_OK) {
		I2C_MSTR_send(0x00); // Tell RTC that we want to start at seconds byte.
	} else { // If RTC is not available, alert user to error and block further execution.
		LCD_clear();
		LCD_printf("Issues communicating\n");
		LCD_printf("with clock.\n\n\n");
		UART_printf(UART_UART0, "GET_DATE: Issues with clock.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	}
	//If it reached this point, should be ready to get.
	if (I2C_MSTR_start(RTC_ADDR, I2C_MODE_MR) == I2C_STAT_OK) {
		I2C_MSTR_get(&sec, TRUE);
		I2C_MSTR_get(&min, TRUE);
		I2C_MSTR_get(&hours, TRUE);
		I2C_MSTR_get(&day, TRUE);
		I2C_MSTR_get(&date, TRUE);
		I2C_MSTR_get(&month, TRUE);
		I2C_MSTR_get(&year, FALSE);
		I2C_MSTR_stop();
		switch (day) {
		case 1:				//Sunday
			strcpy(buf, "Sun");
			break;
		case 2:				//Monday
			strcpy(buf, "Mon");
			break;
		case 3:				//Tuesday
			strcpy(buf, "Tue");
			break;
		case 4:				//Wed
			strcpy(buf, "Wed");
			break;
		case 5:				//Thursday
			strcpy(buf, "Thu");
			break;
		case 6:				//Friday
			strcpy(buf, "Fri");
			break;
		case 7:				//Saturday
			strcpy(buf, "Sat");
			break;
		default:			// End of the world.
			strcpy(buf, "OMG");
			break;
		}
		strcat(buf, " ");
		char tmp[21];
		sprintf(tmp, "%d/%d/20%d %02d:%02d:%02d", BCDtoINT(month),
				BCDtoINT(date), BCDtoINT(year), BCDtoINT(hours), BCDtoINT(min),
				BCDtoINT(sec));
		strcat(buf, tmp);
#ifdef DEBUG
		UART_printf(UART_UART0, "GET DATE BUF: %s\n", buf);
#endif
	} else {
		LCD_clear();
		LCD_printf("Issues reading\n");
		LCD_printf("the clock.\n\n\n");
		UART_printf(UART_UART0, "GET_DATE: Issues with reading the clock.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	}
	return buf;
}

void log_data() {
	char date[30];
#ifdef DEBUG
	UART_printf(UART_UART0, "BUF: %s", date);
#endif

	get_date(date);
	char lData[120];
	sprintf(lData, "%s - %d degrees\n", date, getTemp());
#ifdef DEBUG
	UART_printf(UART_UART0, "LOG_DATA date: %s\n", date);
#endif
	STORE_writes((unsigned char *) lData, strlen(lData));
	return;
}

void transfer_data() {
	unsigned char buf = 0x00;
	unsigned int i = 2;
	I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x00);
	I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
	I2C_MSTR_get(&curByteH, TRUE);
	I2C_MSTR_get(&curByteL, TRUE);
#ifdef DEBUG
	UART_printf(UART_UART0, "curByte: 0x%02x%02x\n", curByteH, curByteL);
#endif
	while (1) {
		if (i > 0x9000) {
#ifdef DEBUG
			UART_printf(UART_UART0, "It's over 9000!\n");
#endif
			break;
		}
		if (i >> 8 > curByteH)
			break;
		if (((i >> 8) == curByteH) && ((i & 0x00ff) >= curByteL))
			break;
		I2C_MSTR_get(&buf, TRUE);
		if (buf != '*')
			UART0_printf("%c", buf);
		i++;
	}
	UART0_printf("EOL\n");
}

int BCDtoINT(unsigned char var) {
	return (((var & 0xf0) >> 4) * 10) + (var & 0x0f);
}

unsigned char INTtoBCD(int var) {
	return ((var / 10) << 4) + (var % 10);
}

// Desc: Get the temp from the sensor, returned in degrees f.

int getTemp() {
	//Reset, skip ROM and start temperature conversion
	OW_reset();
	OW_write_byte(OW_CMD_SKIPROM);
	OW_write_byte(OW_CMD_CONVERTTEMP);

	//Wait until conversion is complete
	while (!OW_read_bit())
		;
	//LCD_printf_RC( 2, 0, "yo");
	//Reset, skip ROM and send command to read Scratchpad
	OW_reset();
	OW_write_byte(OW_CMD_SKIPROM);
	OW_write_byte(OW_CMD_RSCRATCHPAD);

	//Read Scratchpad (only 2 first bytes)
	temperature[0] = OW_read_byte();
	temperature[1] = OW_read_byte();
	OW_reset();

	//Store temperature integer digits and decimal digits
	digit = temperature[0] >> 4;
	digit |= (temperature[1] & 0x7) << 4;

	//Store decimal digits
//	decimal = temperature[0] & 0xf;
	return digit;
}
// Desc: Reset the 1-wire bus, monitor for any device presence.
BOOL ONEWIRE_reset()    //Send reset signal on 1-Wire bus
{
//	uint8_t retries = 125;
	BOOL rval = FALSE; // 0 for device not present, 1 for device(s) present
	cli();
	// Disable interrupts.
	DDRA |= (1 << 3);
	CBV(3, PORTA);
	//ADC3 = PA3
	_delay_us(480);
	_delay_us(32);
	DDRA &= ~(1 << 3);
	if (GBV(3, PORTA) == 0) {
		rval = TRUE;
		UART_printf(UART_UART0, "1-Wire Devices Detected\n");
	} else {
		rval = FALSE;
		UART_printf(UART_UART0, "No 1-Wire Devices Detected!\n");
	}
	DDRA |= (1 << 3);
	sei();
	return rval;
}

void init_eeprom() {
	UART_open(UART_UART0);
	UART_configure(UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 9600);
	UART_set_TX_state(UART_UART0, UART_ENABLE);
	UART0_printf("UART0 enabled.");
#ifdef DEBUG
	UART0_printf(" DEBUG mode on.");
#endif
	UART0_printf("\n");

	if (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK) {
		LCD_open();
		LCD_printf("Error: EEPROM not\n talking.\n");
		if (SYS_get_state(SUBSYS_SPKR) == SUBSYS_CLOSED) {
			SPKR_open(SPKR_BEEP_MODE);
		}
		SPKR_beep(abs(440));
		TMRSRVC_delay(100);
		SPKR_beep(0);
		TMRSRVC_delay(10);
		while (1)
			;
	} else {
		I2C_MSTR_send_multiple(memLocH, 2);
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
		I2C_MSTR_get(&curByteH, TRUE);
		I2C_MSTR_get(&curByteL, FALSE);
		I2C_MSTR_stop();
	}
	if (curByteH > 0x39 || (curByteH == 0x00 && curByteL < 0x02)) //If out of range OR first two bytes
			{
#ifdef DEBUG
		UART_printf(UART_UART0, "ERROR: Resetting log.\n");
#endif
		reset_eeprom();
	}
}
void reset_eeprom() {
	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x00);
	I2C_MSTR_stop();
	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x01);
	I2C_MSTR_send(0x02);
	I2C_MSTR_stop();
	curByteH = 0x00;
	curByteL = 0x02;
}
void STORE_writes(unsigned char *input, int length) {
	unsigned int i;

	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send_multiple(memLocH, 2);
	I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
	I2C_MSTR_get(&curByteH, TRUE);
	I2C_MSTR_get(&curByteL, FALSE);
	I2C_MSTR_stop();

	if (curByteH == 0x00 && (curByteL == 0x00 || curByteL == 0x01))
		curByteL = 0x02; // Invalid write area, reserve first two bytes for curByte.
	else if (curByteH > 0x39) // EEPROM is full....
		prompt_erase();

	for (i = 0; i < length; i++) {
		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send(curByteH);
		I2C_MSTR_send(curByteL);
		I2C_MSTR_send(input[i]);
		I2C_MSTR_stop();
		if (curByteL == 0xFF) {
			curByteL = 0x00;
			curByteH++;
		} else {
			curByteL++;
		}
	}

	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send(curByteH);
	I2C_MSTR_send(curByteL);
	I2C_MSTR_send('*');
	I2C_MSTR_stop();
	if (curByteL == 0xFF) {
		curByteL = 0x00;
		curByteH++;
	} else {
		curByteL++;
	}
	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(curByteH);
	I2C_MSTR_stop();
	while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
		;
	I2C_MSTR_send(0x00);
	I2C_MSTR_send(0x01);
	I2C_MSTR_send(curByteL);
	I2C_MSTR_stop();
}

uint8_t OW_reset() {
	uint8_t i;
	//Pull line low and wait for 480uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(us(480));

	//Release line and wait for 60uS
	OW_INPUT_MODE();
	OW_delay(us( 60));
	//Store line value and wait until the completion of 480uS period
	i = (OW_PIN & (1 << OW_DQ));
	OW_delay(us( 420 ));
	//Return the value read from the presence pulse (0=OK, 1=WRONG)
	return i;
}

void OW_write_bit(uint8_t bit) {
	//Pull line low for 1uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(us( 1));
	//If we want to write 1, release the line (if not will keep low)
	if (bit)
		OW_INPUT_MODE();
	//Wait for 60uS and release the line
	OW_delay(us( 60));
	OW_INPUT_MODE();
}

uint8_t OW_read_bit(void) {
	uint8_t bit = 0;
	//Pull line low for 1uS
	OW_LOW();
	OW_OUTPUT_MODE();
	OW_delay(us( 1));
	//Release line and wait for 14uS
	OW_INPUT_MODE();
	OW_delay(us( 14));
	//Read line value
	if (OW_PIN & (1 << OW_DQ))
		bit = 1;
	//Wait for 45uS to end and return read value
	OW_delay(us( 45));
	return bit;
}

uint8_t OW_read_byte(void) {
	uint8_t i = 8, n = 0;
	while (i--) {
		//Shift one position right and store read value
		n >>= 1;
		n |= (OW_read_bit() << 7);
	}
	return n;
}

void OW_write_byte(uint8_t byte) {
	uint8_t i = 8;
	while (i--) {
		//Write actual bit and shift one position right to make
		//the next bit ready
		OW_write_bit(byte & 0x01);
		byte >>= 1;
	}
}
